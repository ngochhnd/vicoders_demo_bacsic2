<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnepayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('onepay', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_no');
            $table->integer('requirement_id');
            $table->integer('amount');
            $table->string('currency');
            $table->string('local');
            $table->string('merch_txn_ref');
            $table->string('merchant');
            $table->integer('response_code');
            $table->string('secure_hash');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('onepay');
    }
}
