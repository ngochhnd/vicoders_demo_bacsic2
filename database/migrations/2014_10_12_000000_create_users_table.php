<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('first_name');
            $table->string('last_name');
            $table->datetime('birth');
            $table->string('phone_area_code', 5);
            $table->string('phone_number', 20);
            $table->string('address');
            $table->string('avatar')->nullable();
            $table->boolean('gender')->comment('0: female, 1: male');
            $table->text('description');
            $table->integer('card_id');
            $table->integer('card_last_four');
            $table->integer('card_expired');
            $table->string('account_type')->default('normal')->comment('facebook, google, ...');
            $table->string('social_id');
            $table->boolean('email_verified');
            $table->integer('number_review')->default(0);
            $table->double('avg_rating')->default(0);
            $table->integer('order');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
