<?php

use App\Entities\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name'      => 'Cá nhân',
                'parent_id' => 0,
            ],
            [
                'name'      => 'Doanh nghiệp',
                'parent_id' => 0,
            ],
            [
                'name'      => 'Hôn nhân',
                'label'     => 'Hôn nhân gia đình',
                'parent_id' => 1,
            ],
            [
                'name'      => 'Hinh sự',
                'parent_id' => 1,
            ],
            [
                'name'      => 'Thừa kế di chúc',
                'parent_id' => 1,
            ],
            [
                'name'      => 'Dân sự',
                'parent_id' => 1,
            ],
            [
                'name'      => 'Vấn đề khác',
                'parent_id' => 1,
            ],
        ];

        foreach ($categories as $item) {
            $item['slug']     = str_slug($item['name']);
            $category         = Category::updateOrCreate(['name' => $item['name']], $item);
            $category->status = Category::STATUS_ACTIVE;
            $category->save();
        }
    }
}