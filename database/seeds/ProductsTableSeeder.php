<?php

use App\Entities\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'name'    => 'Áo đẹp',
                'image'   => 'images/aodep/jpg',
                'content' => 'ao thiet ke dep',
            ],
            [
                'name'    => 'Áo cực đẹp',
                'image'   => 'images/aodep/jpg',
                'content' => 'ao thiet ke dep',
            ],
        ];

        foreach ($products as $item) {
            $item['slug']    = str_slug($item['name']);
            $product         = Product::updateOrCreate(['name' => $item['name']], $item);
            $product->status = Product::STATUS_ACTIVE;
            $product->save();
        }
    }

}
