<?php

use App\Entities\Lawyer;
use App\Entities\User;
use Illuminate\Database\Seeder;
use NF\Roles\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\en_US\Person($faker));
        $faker->addProvider(new Faker\Provider\en_SG\Address($faker));
        $faker->addProvider(new Faker\Provider\en_US\Company($faker));
        $faker->addProvider(new Faker\Provider\DateTime($faker));
        $faker->addProvider(new Faker\Provider\Internet($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));

        $description = '';
        foreach ($faker->paragraphs() as $paragraph) {
            $description .= $paragraph;
        }

        $users = [
            'chungnd'     => [
                'first_name' => 'Chung',
                'last_name'  => 'Nguyễn',
                'email'      => 'nguyenchung26011992@gmail.com',
                'active'     => 1,
            ],
        ];

        foreach ($users as $item) {
            $user_data = [
                'email'      => $item['email'],
                'first_name' => $item['first_name'],
                'last_name'  => $item['last_name'],
            ];

            $user = User::updateOrCreate([
                'email' => $item['email'],
            ], $user_data);

            $user->password      = Hash::make('secret');
            $user->avatar        = '/uploads/default_avatar.png';
            $user->status        = User::STATUS_ACTIVE;
            $user->description   = $description;
            $user->save();
        }

        for ($i = 0; $i < 20; $i++) {
            if ($i % 2 == 0) {
                $gender = 'male';
            } else {
                $gender = 'female';
            }
            $email = $faker->email();

            $user_data = [
                'email'      => $email,
                'first_name' => $faker->firstName($gender),
                'last_name'  => $faker->lastName($gender),
            ];

            $user = User::updateOrCreate([
                'email' => $email,
            ], $user_data);

            $user->password      = Hash::make('secret');
            $user->avatar        = '/uploads/default_avatar.png';
            $user->status        = User::STATUS_ACTIVE;
            $user->save();
        }
    }
}
