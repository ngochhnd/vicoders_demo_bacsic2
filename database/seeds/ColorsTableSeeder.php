<?php

use App\Entities\Color;
use Illuminate\Database\Seeder;

class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colors = [
            [
                'name'  => 'Xanh',
                'image' => 'blue.jpg',
            ],
            [
                'name'  => 'Đỏ',
                'image' => 'red.jpg',
            ],
        ];

        foreach ($colors as $item) {
            $item['slug']  = str_slug($item['name']);
            $color         = Color::updateOrCreate(['slug' => $item['slug']], $item);
            $color->status = Color::STATUS_ACTIVE;
            $color->save();
        }
    }
}
