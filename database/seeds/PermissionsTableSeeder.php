<?php

use Illuminate\Database\Seeder;
use NF\Roles\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::updateOrCreate([
            'slug' => 'view.roles',
        ], [
            'name'        => 'View Roles',
            'slug'        => 'view.roles',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.roles',
        ], [
            'name'        => 'Create Roles',
            'slug'        => 'create.roles',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.roles',
        ], [
            'name'        => 'Update Roles',
            'slug'        => 'update.roles',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.roles',
        ], [
            'name' => 'Delete Roles',
            'slug' => 'delete.roles',
        ]);

        // Users
        Permission::updateOrCreate([
            'slug' => 'view.users',
        ], [
            'name'        => 'View Users',
            'slug'        => 'view.users',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.users',
        ], [
            'name'        => 'Create Users',
            'slug'        => 'create.users',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.users',
        ], [
            'name'        => 'Update Users',
            'slug'        => 'update.users',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.users',
        ], [
            'name' => 'Delete Users',
            'slug' => 'delete.users',
        ]);

        // Categories
        Permission::updateOrCreate([
            'slug' => 'view.categories',
        ], [
            'name'        => 'View Categories',
            'slug'        => 'view.categories',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.categories',
        ], [
            'name'        => 'Create Categories',
            'slug'        => 'create.categories',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.categories',
        ], [
            'name'        => 'Update Categories',
            'slug'        => 'update.categories',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.categories',
        ], [
            'name' => 'Delete Categories',
            'slug' => 'delete.categories',
        ]);

        // Posts
        Permission::updateOrCreate([
            'slug' => 'view.posts',
        ], [
            'name'        => 'View Posts',
            'slug'        => 'view.posts',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.posts',
        ], [
            'name'        => 'Create Posts',
            'slug'        => 'create.posts',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.posts',
        ], [
            'name'        => 'Update Posts',
            'slug'        => 'update.posts',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.posts',
        ], [
            'name' => 'Delete Posts',
            'slug' => 'delete.posts',
        ]);

        // Testimonials
        Permission::updateOrCreate([
            'slug' => 'view.testimonials',
        ], [
            'name'        => 'View Testimonials',
            'slug'        => 'view.testimonials',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.testimonials',
        ], [
            'name'        => 'Create Testimonials',
            'slug'        => 'create.testimonials',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.testimonials',
        ], [
            'name'        => 'Update Testimonials',
            'slug'        => 'update.testimonials',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.testimonials',
        ], [
            'name' => 'Delete Testimonials',
            'slug' => 'delete.testimonials',
        ]);

        // Careers
        Permission::updateOrCreate([
            'slug' => 'view.careers',
        ], [
            'name'        => 'View Careers',
            'slug'        => 'view.careers',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.careers',
        ], [
            'name'        => 'Create Careers',
            'slug'        => 'create.careers',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.careers',
        ], [
            'name'        => 'Update Careers',
            'slug'        => 'update.careers',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.careers',
        ], [
            'name' => 'Delete Careers',
            'slug' => 'delete.careers',
        ]);

        // Requirements
        Permission::updateOrCreate([
            'slug' => 'view.requirements',
        ], [
            'name'        => 'View Requirements',
            'slug'        => 'view.requirements',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.requirements',
        ], [
            'name'        => 'Create Requirements',
            'slug'        => 'create.requirements',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.requirements',
        ], [
            'name'        => 'Update Requirements',
            'slug'        => 'update.requirements',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.requirements',
        ], [
            'name' => 'Delete Requirements',
            'slug' => 'delete.requirements',
        ]);
    }
}
