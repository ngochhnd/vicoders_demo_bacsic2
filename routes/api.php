<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    $api->post('login', 'App\Http\Controllers\Api\AuthenticateController@authenticate');
    $api->post('register', 'App\Http\Controllers\Api\AuthenticateController@register');

    $api->get('users/all', 'App\Http\Controllers\Api\UserController@getAll');
    $api->resource('users', 'App\Http\Controllers\Api\UserController');

    $api->get('categories/all', 'App\Http\Controllers\Api\CategoryController@getAll');
    $api->resource('categories', 'App\Http\Controllers\Api\CategoryController');

    $api->get('products/all', 'App\Http\Controllers\Api\ProductController@getAll');
    $api->resource('products', 'App\Http\Controllers\Api\ProductController');

    $api->get('posts/all', 'App\Http\Controllers\Api\PostController@getAll');
    $api->resource('posts', 'App\Http\Controllers\Api\PostController');

    $api->get('colors/all', 'App\Http\Controllers\Api\ColorController@getAll');
    $api->resource('colors', 'App\Http\Controllers\Api\ColorController');

});
