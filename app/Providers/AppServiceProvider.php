<?php

namespace App\Providers;

use App\Entities\Post;
use App\Entities\Product;
use App\Repositories\CategoryRepository;
use App\Repositories\CategoryRepositoryEloquent;
use App\Repositories\ColorRepository;
use App\Repositories\ColorRepositoryEloquent;
use App\Repositories\PostRepository;
use App\Repositories\PostRepositoryEloquent;
use App\Repositories\ProductRepository;
use App\Repositories\ProductRepositoryEloquent;
use App\Repositories\UserRepository;
use App\Repositories\UserRepositoryEloquent;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Relation::morphMap([
            'Product' => Product::class,
            'Post'    => Post::class,
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind(UserRepository::class, UserRepositoryEloquent::class);
        App::bind(CategoryRepository::class, CategoryRepositoryEloquent::class);
        App::bind(ProductRepository::class, ProductRepositoryEloquent::class);
        App::bind(PostRepository::class, PostRepositoryEloquent::class);
        App::bind(ColorRepository::class, ColorRepositoryEloquent::class);
    }
}
