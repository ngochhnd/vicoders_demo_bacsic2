<?php

namespace App\Entities;

use App\Entities\Category;
use App\Entities\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    const TYPE_POST       = 0;
    const TYPE_LEGAL_BLOG = 1;

    protected $fillable = [
        'title',
        'slug',
        'image',
        'content',
        'lawyer',
        'answer',
        'user_id',
        'order',
    ];

    public function categories()
    {
        return $this->morphToMany(Category::class, 'categoriable');
    }

    /**
     * Get the user that owns the post.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
