<?php

namespace App\Entities;

use App\Entities\Category;
use App\Entities\Color;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    protected $fillable = [
        'name',
        'image',
        'slug',
        'content',
    ];

    /**
     * The roles that belong to the color.
     */
    public function colors()
    {
        return $this->belongsToMany(Color::class, 'product_color');
    }

    public function categories()
    {
        return $this->morphToMany(Category::class, 'categoriable');
    }
}
