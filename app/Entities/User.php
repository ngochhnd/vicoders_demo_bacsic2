<?php

namespace App\Entities;

use App\Entities\Post;
use Illuminate\Auth\Passwords\CanResetPassword as CanResetPasswordTrait;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use NF\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use NF\Roles\Models\Role;
use NF\Roles\Traits\HasRoleAndPermission;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
class User extends Authenticatable implements HasRoleAndPermissionContract, CanResetPassword
{
    use Notifiable;
    use TransformableTrait;
    use HasRoleAndPermission;
    use CanResetPasswordTrait;

    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    const EMAIL_VERIFIED     = 1;
    const EMAIL_NOT_VERIFIED = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'first_name',
        'last_name',
        'birth',
        'phone_area_code',
        'phone_number',
        'address',
        'avatar',
        'card_id',
        'gender',
        'description',
        'account_type',
        'social_id',
        'number_review',
        'avg_rating',
        'order',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Get the posts for the user.
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * Generate email verify token
     *
     * @return string
     */
    public function getEmailVerifyToken()
    {
        return Hash::make($this->email);
    }

     public function getToken()
    {
        return JWTAuth::fromUser($this);
    }

}
