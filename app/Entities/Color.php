<?php

namespace App\Entities;

use App\Entities\Product;
use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    protected $fillable = [
        'name',
        'slug',
        'image',
    ];

    /**
     * The users that belong to the prouct.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_color');
    }
}
