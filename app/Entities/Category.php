<?php

namespace App\Entities;

use App\Entities\Product;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";

    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    const EMAIL_VERIFIED     = 1;
    const EMAIL_NOT_VERIFIED = 0;

    protected $fillable = [
        'name',
        'image',
        'slug',
        'order',
        'parent_id',
        'label',
        'description',
    ];

    public function products()
    {
        return $this->morphedByMany(Product::class, "categoriable");
    }

    public function posts()
    {
        return $this->morphedByMany(Post::class, 'categoriable');
    }
}
