<?php

namespace App\Notifications;

use App\Mail\CustomEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SenEmailWhenUserRegisteredNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // $email = $notifiable->email;
        // return (new MailMessage)
        //     ->from($email)
        //     ->line('The introduction to the notification.')
        //     ->action('Notification Action', url('/'))
        //     ->line('Thank you for using our application!');

        $token = $notifiable->getEmailVerifyToken();
        return (new CustomEmail($notifiable))
            ->to($notifiable->email)
            ->subject('Đăng ký người dùng')
            ->line("Cảm ơn bạn đã đăng ký.")
            ->line('<span style="color: #dc4d2f">Nhấn vào nút này để xác thực email của bạn</span> ' . $notifiable->email)
            ->action('Xác thực email', asset("verify?id={$notifiable->id}&token={$token}"));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
