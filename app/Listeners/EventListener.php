<?php

namespace App\Listeners;

use App\Events\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(Event $event)
    {
        $user = $event->user;
         // Tam dung 10 phut
        // sleep(600);
    
        // echo $event->user->id;
        // echo $event->user->last_name;
        // $fileName = $event->user->id . '.txt';
        // $data = 'Sản phẩm mới tạo: ' . $event->user->last_name . ' với ID: ' . $event->user->id; 
        // File::put(public_path('/txt/' . $fileName), $data);
        echo "đã gửi email";
        return true;
        // $user->notify(new UserRegisteredNotification($user));
    }
}
