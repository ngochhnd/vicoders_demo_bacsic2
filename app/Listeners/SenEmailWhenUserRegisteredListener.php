<?php

namespace App\Listeners;

use App\Events\UserRegisterEvent;
use App\Notifications\SenEmailWhenUserRegisteredNotification;

class SenEmailWhenUserRegisteredListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserRegisterEvent $event)
    {
        $user = $event->user;

        $user->notify(new SenEmailWhenUserRegisteredNotification($user));
    }
}
