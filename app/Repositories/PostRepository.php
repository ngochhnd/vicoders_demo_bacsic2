<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PostRepository
 * @package namespace App\Repositories;
 */
interface PostRepository extends RepositoryInterface
{
    
}
