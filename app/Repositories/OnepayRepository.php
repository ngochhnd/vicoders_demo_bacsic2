<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OnepayRepository
 * @package namespace App\Repositories;
 */
interface OnepayRepository extends RepositoryInterface
{
    //
}
