<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\OnepayRepository;
use App\Entities\Onepay;

/**
 * Class OnepayRepositoryEloquent
 * @package namespace App\Repositories;
 */
class OnepayRepositoryEloquent extends BaseRepository implements OnepayRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Onepay::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
