<?php

namespace App\Repositories;

use App\Entities\Lawyer;
use App\Entities\User;
use App\Repositories\CanFlushCache;
use App\Repositories\UserRepository;
use NF\Roles\Models\Role;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class UserRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository, CacheableInterface
{
    use CacheableRepository, CanFlushCache;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function existsEmail($id, $email)
    {
        $user = $this->findWhere([
            'email' => $email,
            ['id', '!=', $id],
        ])->first();
        if (!$user) {
            return false;
        } else {
            return $user;
        }
    }

    public function attachRole($role, $id)
    {
        $user = $this->find($id);
        if ($user->roles->isEmpty()) {
            $role = Role::where('slug', $role)->first();
            $user->attachRole($role);
            // throw new AccessDeniedHttpException("can not change the role of user", null, 1027);
        }
        return $user;
    }

    public function createLawyerProfile($data, $id)
    {
        if (isset($data['question_salary'])) {
            $attributes['question_salary'] = $data['question_salary'];
        }
        if (isset($data['meeting_salary'])) {
            $attributes['meeting_salary'] = $data['meeting_salary'];
        }
        if (isset($data['phone_salary'])) {
            $attributes['phone_salary'] = $data['phone_salary'];
        }
        if (isset($data['education'])) {
            $attributes['education'] = $data['education'];
        }
        if (isset($data['certificate'])) {
            $attributes['certificate'] = $data['certificate'];
        }
        if (isset($data['language'])) {
            $attributes['language'] = $data['language'];
        }
        if (isset($data['experience'])) {
            $attributes['experience'] = $data['experience'];
        }
        if (isset($data['practicing_certificate'])) {
            $attributes['practicing_certificate'] = $data['practicing_certificate'];
        }
        if (isset($data['offline_consulting_address'])) {
            $attributes['offline_consulting_address'] = $data['offline_consulting_address'];
        }
        if (isset($data['owner_bank_name'])) {
            $attributes['owner_bank_name'] = $data['owner_bank_name'];
        }
        if (isset($data['bank_name'])) {
            $attributes['bank_name'] = $data['bank_name'];
        }
        if (isset($data['bank_branch'])) {
            $attributes['bank_branch'] = $data['bank_branch'];
        }
        if (isset($data['bank_number'])) {
            $attributes['bank_number'] = $data['bank_number'];
        }
        if (isset($data['note'])) {
            $attributes['note'] = $data['note'];
        }
        if (isset($data['is_answer_48h'])) {
            $attributes['is_answer_48h'] = $data['is_answer_48h'];
        }
        if (isset($data['is_feedback_asap'])) {
            $attributes['is_feedback_asap'] = $data['is_feedback_asap'];
        }
        if (isset($data['is_answer_free_question'])) {
            $attributes['is_answer_free_question'] = $data['is_answer_free_question'];
        }
        if (isset($data['is_legal_document_formulation'])) {
            $attributes['is_legal_document_formulation'] = $data['is_legal_document_formulation'];
        }
        if (isset($data['is_display_homepage'])) {
            $attributes['is_display_homepage'] = $data['is_display_homepage'];
        }
        if (isset($data['province_id'])) {
            $attributes['province_id'] = $data['province_id'];
        }
        if (isset($data['district_id'])) {
            $attributes['district_id'] = $data['district_id'];
        }

        $attributes['user_id'] = $id;
        $lawyer                = Lawyer::create($attributes);
        return $lawyer;
    }

    public function updateLawyerProfile($data, $user)
    {
        return $user->lawyer->update($data);
    }

    public function verifyEmail($user)
    {
        $user->email_verified = 1;
        $user->save();
        $this->flushCache();
        return $user;
    }
}
