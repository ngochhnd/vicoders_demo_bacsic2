<?php

namespace App\Validators;

use App\Validators\AbstractValidator;
use App\Validators\ValidatorInterface;

class ProductValidator extends AbstractValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE             => [
            'name'        => ['required', 'max:40'],
            'color_id'    => ['required'],
            'category_id' => ['required'],
        ],
        ValidatorInterface::RULE_UPDATE             => [
            'name'        => ['required', 'max:40'],
            'color_id'    => ['required'],
            'category_id' => ['required'],
        ],
        ValidatorInterface::CHANGE_STATUS_ALL_ITEMS => [
            'item_ids' => ['required'],
            'status'   => ['required'],
        ],
        ValidatorInterface::CHANGE_STATUS_ITEM      => [
            'status' => ['required'],
        ],
    ];
}
