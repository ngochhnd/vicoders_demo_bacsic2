<?php

namespace App\Transformers;

use App\Entities\User;
use App\Transformers\CareerTransformer;
use App\Transformers\CategoryTransformer;
use App\Transformers\ProfileUpdateTransformer;
use App\Transformers\RoleTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class UserTransformer
 * @package namespace App\Transformers;
 */
class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'roles',
        'lawyer',
        'careers',
        'categories',
        'profile_update',
    ];

    public function __construct($includes = [])
    {
        $this->setDefaultIncludes($includes);
    }

    /**
     * Transform the \User entity
     * @param \User $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        $avatar = '';
        if ($model->avatar != '') {
            $avatar = filter_var($model->avatar, FILTER_VALIDATE_URL) === false ? asset($model->avatar) : $model->avatar;
        }

        return [
            'id'              => (int) $model->id,
            'email'           => $model->email,
            'first_name'      => $model->first_name,
            'last_name'       => $model->last_name,
            'name'            => $model->first_name . " " . $model->last_name,
            'birth'           => $model->birth != '0000-00-00 00:00:00' ? $model->birth : null,
            'phone_area_code' => $model->phone_area_code,
            'phone_number'    => $model->phone_number,
            'address'         => $model->address,
            'avatar'          => $avatar,
            'gender'          => $model->gender,
            'description'     => $model->description,
            'card_id'         => $model->card_id,
            'card_last_four'  => $model->card_last_four,
            'card_expired'    => $model->card_expired,
            'number_review'   => $model->number_review,
            'avg_rating'      => $model->avg_rating,
            'order'           => (int) $model->order,
            'status'          => (int) $model->status,
            'pivot'           => $model->pivot,
            'created_at'      => $model->created_at,
            'updated_at'      => $model->updated_at,
        ];
    }

    public function includeRoles(User $model)
    {
        return $this->collection($model->roles, new RoleTransformer);
    }

    public function includeLawyer(User $model)
    {
        if (!empty($model->lawyer)) {
            return $this->item($model->lawyer, new LawyerTransformer);
        }
    }

    public function includeCareers(User $model)
    {
        return $this->collection($model->careers, new CareerTransformer);
    }

    public function includeCategories(User $model)
    {
        return $this->collection($model->categories, new CategoryTransformer);
    }

    public function includeProfileUpdate(User $model)
    {
        if (!empty($model->profile_update)) {
            return $this->item($model->profile_update, new ProfileUpdateTransformer);
        }
    }
}
