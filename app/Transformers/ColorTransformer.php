<?php

namespace App\Transformers;

use App\Entities\Color;
use League\Fractal\TransformerAbstract;

/**
 * Class ColorTransformer
 * @package namespace App\Transformers;
 */
class ColorTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'parent',
    ];

    public function __construct($includes = [])
    {
        $this->setDefaultIncludes($includes);
    }

    /**
     * Transform the Color entity
     * @param App\Entities\Color $model
     *
     * @return array
     */
    public function transform(Color $model)
    {
        $image = '';
        if ($model->image != '') {
            $image = filter_var($model->image, FILTER_VALIDATE_URL) === false ? asset($model->image) : $model->image;
        }

        return [
            'id'          => (int) $model->id,
            'name'        => $model->name,
            'slug'        => $model->slug,
            'status'      => $model->status,
            'created_at'  => $model->created_at,
            'updated_at'  => $model->updated_at,
        ];
    }

    public function includeParent(Color $model)
    {
        if (!empty($model->parent)) {
            return $this->item($model->parent, new ColorTransformer);
        }
    }
}
