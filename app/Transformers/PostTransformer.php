<?php

namespace App\Transformers;

use App\Entities\Post;
use App\Transformers\CategoryTransformer;
use App\Transformers\CommentTransformer;
use App\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class PostTransformer
 * @package namespace App\Transformers;
 */
class PostTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'user',
        'categories',
        'comments',
    ];

    protected $commentTransformerDefaultIncludes = [];

    public function __construct($includes = [])
    {
        $this->setDefaultIncludes($includes);
    }

    /**
     * Transform the \Post entity
     * @param \Post $model
     *
     * @return array
     */
    public function transform(Post $model)
    {
        $image = '';
        if ($model->image != '') {
            $image = filter_var($model->image, FILTER_VALIDATE_URL) === false ? asset($model->image) : $model->image;
        }

        return [
            'id'         => (int) $model->id,
            'title'      => $model->title,
            'slug'       => $model->slug,
            'image'      => $image,
            'content'    => $model->content,
            'lawyer'     => $model->lawyer,
            'answer'     => $model->answer,
            'user_id'    => (int) $model->user_id,
            'status'     => (int) $model->status,
            'order'      => (int) $model->order,
            'type'       => (int) $model->type,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }

    public function includeUser(Post $model)
    {
        if (!empty($model->user)) {
            return $this->item($model->user, new UserTransformer);
        }
    }

    public function includeCategories(Post $model)
    {
        if (!empty($model->categories)) {
            return $this->collection($model->categories, new CategoryTransformer);
        }
    }

    public function setCommentTransfomerDefaultIncludes($includes)
    {
        $this->commentTransformerDefaultIncludes = $includes;
    }

    public function includeComments(Post $model)
    {
        if (!empty($model->comments)) {
            return $this->collection($model->comments, new CommentTransformer($this->commentTransformerDefaultIncludes));
        }
    }
}
