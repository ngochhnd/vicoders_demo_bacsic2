<?php

namespace App\Http\Controllers\Api;

use App\Entities\Product;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\ProductRepository;
use App\Transformers\ProductTransformer;
use App\Validators\ProductValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ProductController extends ApiController
{
    private $repository;
    private $validator;
    public $per_page = 15;

    public function __construct(ProductRepository $repository, ProductValidator $validator)
    {
        $this->validator  = $validator;
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $product  = new Product;
        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        // $products = $this->repository->findWhere(['status' => Product::STATUS_ACTIVE]);
        // $products = $this->repository->findWhere(['status' => Product::STATUS_ACTIVE])->paginate($this->per_page);
        return $this->response->paginator($products, new ProductTransformer);
    }

    public function getAll()
    {
        $product  = new Product;
        $products = $product->where('status', Product::STATUS_ACTIVE)->get();
        return $this->response->collection($products, new ProductTransformer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator->isValid($request, 'RULE_CREATE');

        $data         = $request->all();
        $data['slug'] = $this->_getSlug($this->repository, $data['name']);
        $product      = $this->repository->create($data);
        $product->save();
        $product->colors()->attach($data["color_id"]);
        $product->categories()->sync($data["category_id"]);

        return $this->response->item($product, new ProductTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validator->isValid($request, 'RULE_UPDATE');

        $data         = $request->all();
        $data['slug'] = $this->_getSlug($this->repository, $data['name']);
        $product      = $this->repository->update($data, $id);
        $product->colors()->attach($data["color_id"]);
        return $this->response->item($product, new ProductTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->repository->find($id);
        if (!$product) {
            throw new \Exception("Product not found", 1);
        }
        $product->colors()->detach();
        $product->categories()->detach();
        $this->repository->delete($id);
        return $this->success();
    }
}
