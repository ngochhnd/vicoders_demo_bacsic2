<?php

namespace App\Http\Controllers\Api;

use App\Entities\Post;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\PostRepository;
use App\Transformers\PostTransformer;
use App\Validators\PostValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class PostController extends ApiController
{
    private $repository;
    private $validator;
    public $per_page = 15;

    public function __construct(PostRepository $repository, PostValidator $validator)
    {
        $this->validator  = $validator;
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $post     = new Post;
        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $posts    = $post->where('status', Post::STATUS_ACTIVE)->paginate($per_page);
        return $this->response->paginator($posts, new PostTransformer);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        $post  = new Post;
        $posts = $post->where('status', Post::STATUS_ACTIVE)->get();
        return $this->response->collection($posts, new PostTransformer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator->isValid($request, 'RULE_CREATE');

        $data         = $request->all();
        $data['slug'] = $this->_getSlug($this->repository, $data['title']);
        $post         = $this->repository->create($data);
        $post->status = Post::STATUS_ACTIVE;
        $post->save();
        // $post->categories()->syncWithoutDetaching(1);
        $post->categories()->sync($data['category_id']);

        return $this->response->item($post, new PostTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validator->isValid($request, 'RULE_UPDATE');

        $data         = $request->all();
        $data['slug'] = $this->_getSlug($this->repository, $data['title']);
        $post         = $this->repository->update($data, $id);

        return $this->response->item($post, new PostTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = $this->repository->find($id);
        if (!$post) {
            throw new \Exception("Post not found", 1);
        }
        $post->categories()->detach();
        $this->repository->delete($id);
        return $this->success();
    }
}
