<?php

namespace App\Http\Controllers\Api;

use App\Entities\Category;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\CategoryRepository;
use App\Transformers\CategoryTransformer;
use App\Validators\CategoryValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class CategoryController extends ApiController
{
    private $repository;
    private $validator;
    public $per_page = 15;

    public function __construct(CategoryRepository $repository, CategoryValidator $validator)
    {
        $this->validator  = $validator;
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category  = new Category;
        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $categorys = $category->where('status', Category::STATUS_ACTIVE)->paginate($per_page);
        return $this->response->paginator($categorys, new CategoryTransformer);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        $category  = new Category;
        $categorys = $category->where('status', Category::STATUS_ACTIVE)->get();
        return $this->response->collection($categorys, new CategoryTransformer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator->isValid($request, 'RULE_CREATE');
        $data             = $request->all();
        $data['slug']     = $this->_getSlug($this->repository, $data['name']);
        $category         = $this->repository->create($data);
        $category->status = Category::STATUS_ACTIVE;
        $category->save();

        return $this->response->item($category, new CategoryTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validator->isValid($request, 'RULE_UPDATE');

        $data         = $request->all();
        $data['slug'] = $this->_getSlug($this->repository, $data['name']);
        $category     = $this->repository->update($data, $id);

        return $this->response->item($category, new CategoryTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = $this->repository->find($id);
        if (!$category) {
            throw new Exception("Ctegory not found", 1);
        }

        if (count($category->products) > 0) {
            $category->products()->detach();
        }
        if (count($category->posts) > 0) {
            $category->posts()->detach();
        }
        
        $this->repository->delete($id);
        return $this->success();
    }
}
