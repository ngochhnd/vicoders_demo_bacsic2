<?php

namespace App\Http\Controllers\Api;

use App\Entities\User;
use App\Events\UserRegisterEvent;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthenticateController extends ApiController
{
    private $repository;
    private $validator;

    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->validator  = $validator;
        $this->repository = $repository;
    }

    public function authenticate(Request $request)
    {

        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        $user        = User::where('email', $credentials['email'])->where('status', User::STATUS_ACTIVE)->first();
        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // // all good so return the token
        return response()->json(compact('token'));
    }

    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function register(Request $request)
    {
        $user = User::findOrFail(22);

        // $this->validator->isValid($request, 'RULE_CREATE');

        // $user                 = User::create($request->all());
        // $user->password       = Hash::make($request->password);
        // $user->email_verified = User::EMAIL_VERIFIED;
        // $user->status         = User::STATUS_ACTIVE;
        // $user->save();

        // return $this->response->item($user, new UserTransformer);
        // $user->notify(new SenEmailWhenUserRegisteredNotification($user));
        Event::fire(new UserRegisterEvent($user));
        echo "OK";
        die;
    }
}
