<?php

namespace App\Http\Controllers\Api;

use App\Entities\Color;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\ColorRepository;
use App\Transformers\ColorTransformer;
use App\Validators\ColorValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ColorController extends ApiController
{
    private $repository;
    private $validator;
    public $per_page = 15;

    public function __construct(ColorRepository $repository, ColorValidator $validator)
    {
        $this->validator  = $validator;
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $color  = new Color;
        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $colors = $color->where('status', Color::STATUS_ACTIVE)->paginate($per_page);
        return $this->response->paginator($colors, new ColorTransformer);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        $color  = new Color;
        $colors = $color->where('status', Color::STATUS_ACTIVE)->paginate($this->per_page);
        return $this->response->paginator($colors, new ColorTransformer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator->isValid($request, 'RULE_CREATE');

        $data          = $request->all();
        $data['slug']  = $this->_getSlug($this->repository, $data['name']);
        $color         = $this->repository->create($data);
        $color->status = Color::STATUS_ACTIVE;
        $color->save();

        return $this->response->item($color, new ColorTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validator->isValid($request, 'RULE_UPDATE');

        $data         = $request->all();
        $data['slug'] = $this->_getSlug($this->repository, $data['name']);
        $color        = $this->repository->update($data, $id);

        return $this->response->item($color, new ColorTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $color = $this->repository->find($id);
        if (!$color) {
            throw new \Exception("Color not found", 1);
        }
        $this->repository->delete($id);
        return $this->success();
    }
}
