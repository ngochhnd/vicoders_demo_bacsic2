<?php

namespace App\Http\Controllers\Api;

use App\Entities\Post;
use App\Entities\User;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Redirect;
use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use App\Validators\UserValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class UserController extends ApiController
{
    private $repository;
    private $validator;

    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->validator  = $validator;
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user  = new User;
        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $users = $user->where('status', User::STATUS_ACTIVE)->paginate($per_page);
        return $this->response->paginator($users, new UserTransformer);
    }

    /**
     * get all user where status
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        $user  = new User;
        $users = $user->where('status', User::STATUS_ACTIVE)->get();
        return $this->response->collection($users, new UserTransformer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator->isValid($request, 'RULE_CREATE');

        $data                 = $request->all();
        $user                 = $this->repository->create($data);
        $user->password       = Hash::make($request->password);
        $user->status         = User::STATUS_ACTIVE;
        $user->email_verified = User::EMAIL_VERIFIED;
        $user->save();

        return $this->response->item($user, new UserTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validator->isValid($request, 'RULE_UPDATE');
        $attributes = $request->all();

        $user = $this->repository->update($attributes, $id);
        return $this->response->item($user, new UserTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->repository->find($id);
        if (!$user) {
            throw new \Exception("User not found", 1);
        }
        $post = Post::where('user_id', $id)->get();
        if (count($post) > 0) {
            throw new \Exception("User đang được sử dụng, không thể xóa", 1);
        }
        $this->repository->delete($id);
        return $this->success();
    }

    /*
     * xác thực đăng ký
     */
    public function verify()
    {
        if (!isset($_GET["id"]) || !isset($_GET["token"])) {
            return \Redirect::back()->withErrors(['msg', 'The Message']);
        }
        $user = User::findOrFail($_GET["id"]);
        echo  "---".$user->getToken();
        die;
        return $this->response->array(['token' => $user->token]);
        // $user = User::findOrFail($_GET["id"]);
        // if($user->token = $_GET["token"]){
        //     echo 1;
        // }

        // echo $_GET["id"];
        // echo "<br>" . $_GET["token"];
    }
}
